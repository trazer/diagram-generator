import argparse
import os
import random
import re
import shutil
import string

import chevron
from ruamel.yaml import YAML

from .generator import (
    Dimensions,
    Io,
    Job,
    Pipeline,
    Stage,
    Variant,
)

_CURRENT_PATH = os.path.split(__file__)[0]

_TEMPLATES_PATH = os.path.join(_CURRENT_PATH, 'templates')

DEF_LDEL = '{{=<%'

DEF_RDEL = '%>=}}'


def main():

    print(f'Current work directory: {os.getcwd()}')

    arguments = parse_arguments()

    os.makedirs(arguments.outputPath, exist_ok=True)

    shutil.rmtree(os.path.join(arguments.outputPath, 'images'))
    shutil.copytree(
        arguments.imagesPath,
        os.path.join(arguments.outputPath, 'images'),
        ignore=lambda src, names: set(name for name in names if not name.lower().endswith('.pdf'))
    )

    config = load_config(arguments)

    variants = []
    for variantFromConfig in config['variants']:
        variant = Variant()
        variant.name = variantFromConfig['name']
        variant.triggers = variantFromConfig['triggers']
        variant.fileName = variantFromConfig['file_name']
        variants.append(variant)

    for variant in variants:
        pipeline = Pipeline(arguments.outputPath)
        prepare_pipeline(config, variant, pipeline)
        renderedPipeline = render(pipeline)
        renderedPipeline = escape_invalid_latex_characters_rendered_by_mustache(renderedPipeline)
        with open(os.path.join(arguments.outputPath, variant.fileName + '.tex'), mode='wt') as file:
            file.write(renderedPipeline)

    shutil.copyfile(
        os.path.join(_TEMPLATES_PATH, 'whitecheckmark.pdf'),
        os.path.join(arguments.outputPath, 'whitecheckmark.pdf')
    )


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--config-file',
        metavar='CONFIG_FILE',
        dest='configFilePath',
        type=str,
        required=True,
    )

    parser.add_argument(
        '--images-path',
        metavar='IMAGES_PATH',
        dest='imagesPath',
        type=str,
        required=True,
    )

    parser.add_argument(
        '--pdflatex-executable',
        metavar='PDF_LATEX_EXECUTABLE',
        dest='pdfLatexExecutable',
        type=str,
        required=True,
    )

    parser.add_argument(
        '--output-path',
        metavar='OUTPUT_PATH',
        dest='outputPath',
        type=str,
        required=True,
    )

    return parser.parse_args()


def load_config(arguments):
    yaml = YAML(typ='safe')
    with open(arguments.configFilePath, 'rt') as file:
        return yaml.load(file)


def prepare_pipeline(config, variant: Variant, pipeline: Pipeline):

    pipeline.dimensions = Dimensions()
    for var, key in Dimensions().VAR_2_KEY.items():
        setattr(pipeline.dimensions, var, str(config['dimensions'][key]))

    stages = pipeline.stages
    for stageFromConfig in config['stages']:
        if _skip_stage(stageFromConfig, variant):
            continue
        stage = Stage(pipeline)
        stage.name = stageFromConfig['name']
        stage.stageId = _get_id()
        stages.append(stage)
        assert len(stageFromConfig['jobs']) > 0
        for jobIndex, jobFromConfig in enumerate(stageFromConfig['jobs']):
            job = Job(pipeline, stage)
            job.name = jobFromConfig['name']
            if jobIndex == 0:
                job.jobId = stage.stageId
            else:
                job.jobId = _get_id()
            stage.jobs.append(job)
        for ioFromConfig in stageFromConfig['ios']:
            io = Io(pipeline, stage)
            io.ioId = _get_id()
            io.name = ioFromConfig['name']
            io.image = os.path.join('images', ioFromConfig['image'])
            io.shape = ioFromConfig['shape']
            io.direction = ioFromConfig['direction']
            stage.ios.append(io)


def _skip_stage(stageFromConfig, variant: Variant) -> bool:
    return len(set(stageFromConfig['triggers']).intersection(variant.triggers)) == 0


def _get_id() -> str:
    return ''.join(random.choices(string.ascii_lowercase, k=10))


def render(pipeline: Pipeline):
    renderedPipelineStages = render_pipeline_stages(pipeline)
    renderedPipelineTexts = render_pipeline_texts(pipeline)
    renderedPipelineArrows = render_pipeline_arrows(pipeline)
    renderedPipelineStagesNumbering = render_pipeline_stages_numbering(pipeline)
    renderedIosStages = render_ios_stages(pipeline)
    renderedIosImages = render_ios_images(pipeline)
    renderedIosArrows = render_ios_arrows(pipeline)
    renderedIosTexts = render_ios_texts(pipeline)
    with open(os.path.join(_TEMPLATES_PATH, 'main.tex.mustache'), 'r') as file:
        return chevron.render(
            template=replace_mustache_tags(''.join(file.readlines())),
            data={
                **pipeline.dimensions.to_mustache(),
                'pipeline_stages': renderedPipelineStages,
                'pipeline_texts': renderedPipelineTexts,
                'pipeline_arrows': renderedPipelineArrows,
                'pipeline_stages_numbering': renderedPipelineStagesNumbering,
                'ios_stages': renderedIosStages,
                'ios_images': renderedIosImages,
                'ios_arrows': renderedIosArrows,
                'ios_texts': renderedIosTexts,
            },
            def_ldel=DEF_LDEL,
            def_rdel=DEF_RDEL,
        )


def render_pipeline_stages(pipeline: Pipeline) -> str:
    return _render_template(pipeline, 'pipeline_stages.mustache')


def render_pipeline_texts(pipeline: Pipeline) -> str:
    return _render_template(pipeline, 'pipeline_texts.mustache')


def render_pipeline_arrows(pipeline: Pipeline) -> str:
    return _render_template(pipeline, 'pipeline_arrows.mustache')


def render_pipeline_stages_numbering(pipeline: Pipeline) -> str:
    return _render_template(pipeline, 'pipeline_stages_numbering.mustache')


def render_ios_stages(pipeline: Pipeline) -> str:
    return _render_template(pipeline, 'ios_stages.mustache')


def render_ios_images(pipeline: Pipeline) -> str:
    return _render_template(pipeline, 'ios_images.mustache')


def render_ios_arrows(pipeline: Pipeline) -> str:
    return _render_template(pipeline, 'ios_arrows.mustache')


def render_ios_texts(pipeline: Pipeline) -> str:
    return _render_template(pipeline, 'ios_texts.mustache')


def _render_template(pipeline: Pipeline, templateName: str) -> str:
    with open(os.path.join(_TEMPLATES_PATH, templateName), 'r') as file:
        return chevron.render(
            template=replace_mustache_tags(''.join(file.readlines())),
            data=pipeline.to_mustache(),
            def_ldel=DEF_LDEL,
            def_rdel=DEF_RDEL,
        )


def replace_mustache_tags(template: str):
    return re.sub(
        r'{{(?P<key>[#/^]?(?:[A-Za-z0-9_]*?))}}',
        '{}\g<key>{}'.format(DEF_LDEL, DEF_RDEL),
        template,
    )


def escape_invalid_latex_characters_rendered_by_mustache(renderedTemplate: str) -> str:
    renderedTemplate = re.sub(r'&gt;', '>', renderedTemplate)
    renderedTemplate = re.sub(r'&amp;amp;', '\&', renderedTemplate)
    return renderedTemplate
