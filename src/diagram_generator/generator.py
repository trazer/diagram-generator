from __future__ import annotations

import os
import re
from collections.abc import MutableSequence
from typing import (
    Tuple,
)

from PyPDF2 import PdfFileReader


class Pipeline:

    def __init__(self, outputPath: str):
        self.stages = Stages()
        self.dimensions: Dimensions = None
        self.outputPath = outputPath

    def to_mustache(self) -> dict:
        data = {
            'first_stage_id': self.firstStageId,
            'last_stage_id': self.lastStageId,
            **self.dimensions.to_mustache(),
            'stages': self.stages.to_mustache(),
        }
        return data

    @property
    def firstStageId(self) -> str:
        return self.stages[0].stageId

    @property
    def lastStageId(self) -> str:
        return self.stages[-1].stageId


class Dimensions:

    VAR_2_KEY = {
        'horizontalDistanceBetweenStartAndFirstStage': 'horizontal_distance_between_start_and_first_stage',
        'horizontalDistanceBetweenStages': 'horizontal_distance_between_stages',
        'horizontalDistanceBetweenLastStageAndEnd': 'horizontal_distance_between_last_stage_and_end',
        'verticalDistanceBetweenJobs': 'vertical_distance_between_jobs',
        'horizontalDistanceBetweenJobAndBentArrow': 'horizontal_distance_between_job_and_bent_arrow',
        'jobBentArrowRadius': 'job_bent_arrow_radius',
        'verticalDistanceBetweenLowestJobAndPipelineStageNumbering':
            'vertical_distance_between_lowest_job_and_pipeline_stage_numbering',
        'verticalDistanceBetweenPipelineStageNumberingAndArtifact':
            'vertical_distance_between_pipeline_stage_numbering_and_artifact',
        'jobDiameter': 'job_diameter',
        'stageTextWidth': 'stage_text_width',
        'jobTextWidth': 'job_text_width',
        'startEndWidth': 'start_end_width',
        'startEndHeight': 'start_end_height',
        'verticalDistanceBetweenStageAndStageText': 'vertical_distance_between_stage_and_stage_text',
        'verticalDistanceBetweenJobAndJobText': 'vertical_distance_between_job_and_job_text',
        'horizontalDistanceBetweenIos': 'horizontal_distance_between_ios',
        'ioStageWidth': 'io_stage_width',
        'ioStageHeight': 'io_stage_height',
        'ioTextWidth': 'io_text_width',
        'verticalDistanceBetweenIoAndIoText': 'vertical_distance_between_io_and_io_text',
    }

    def __init__(self):
        self.horizontalDistanceBetweenStartAndFirstStage = None
        self.horizontalDistanceBetweenStages = None
        self.horizontalDistanceBetweenLastStageAndEnd = None
        self.verticalDistanceBetweenJobs = None
        self.horizontalDistanceBetweenJobAndBentArrow = None
        self.jobBentArrowRadius = None
        self.verticalDistanceBetweenLowestJobAndPipelineStageNumbering = None
        self.verticalDistanceBetweenPipelineStageNumberingAndArtifact = None
        self.verticalDistanceBetweenPipelineAndArtifact = None
        self.jobDiameter = None
        self.stageTextWidth = None
        self.jobTextWidth = None
        self.jobWidth = None
        self.jobHeight = None
        self.startEndWidth = None
        self.startEndHeight = None
        self.verticalDistanceBetweenStageAndStageText = None
        self.verticalDistanceBetweenJobAndJobText = None
        self.horizontalDistanceBetweenIos = None
        self.ioStageWidth = None
        self.ioStageHeight = None
        self.ioTextWidth = None
        self.verticalDistanceBetweenIoAndIoText = None

    def to_mustache(self) -> dict:
        return {key: getattr(self, var) for var, key in self.VAR_2_KEY.items()}


class Variant:

    def __init__(self):
        self.name: str = None
        self.triggers = []
        self.fileName: str = None


class Stages(MutableSequence):

    def __init__(self):
        super().__init__()
        self._stages = []

    def __getitem__(self, index: int):
        return self._stages[index]

    def __setitem__(self, index: int, item):
        self._stages[index] = item

    def __delitem__(self, index: int):
        del self._stages[index]

    def __len__(self):
        return len(self._stages)

    def insert(self, index: int, item):
        self._stages.insert(index, item)

    def by_id(self, stageId) -> Stage:
        for stage in self._stages:
            if stage.stageId != stageId:
                continue
            return stage
        raise ValueError(stageId)

    def to_mustache(self) -> list:
        data = []
        for stage in self._stages:
            data.append(stage.to_mustache())
        return data


class Stage:

    def __init__(self, pipeline: Pipeline):
        self.pipeline = pipeline
        self.name = None
        self.stageId = None
        self.jobs = Jobs()
        self.ios = Ios()

    def to_mustache(self) -> dict:
        return {
            **self.pipeline.dimensions.to_mustache(),
            'stage_name': self.name,
            'stage_id': self.stageId,
            'is_first_stage': self.isFirst,
            'is_last_stage': self.isLast,
            'previous_stage_id': self.previousStageId,
            'next_stage_id': self.nextStageId,
            'has_ios': self.hasIos,
            'jobs': self.jobs.to_mustache(),
            'ios': self.ios.to_mustache(),
            'first_job_id': self.jobs[0].jobId,
            'last_job_id': self.jobs[-1].jobId,
            'lowest_overall_job_id': self.lowestOverallJobId,
            'stage_number': self.stageNumber,
            'io_mux_width': self.ioMuxWidth,
            **{f'has_{str(n)}_ios': len(self.ios) == n for n in range(1, 4)},
            **{f'io{str(i+1)}': io.to_mustache() for i, io in enumerate(self.ios)},
            **{f'io{str(i+1)}_id': io.ioId for i, io in enumerate(self.ios)},
        }

    @property
    def ioMuxWidth(self) -> str:
        ioStageWidthDimension, ioStageWidthUnit = split_dimension_and_unit(
            self.pipeline.dimensions.ioStageWidth)
        horizontalDistanceBetweenIosDimension, horizontalDistanceBetweenIosUnit = split_dimension_and_unit(
            self.pipeline.dimensions.horizontalDistanceBetweenIos)
        assert ioStageWidthUnit == horizontalDistanceBetweenIosUnit
        # The 1.05 factor is to have the mux slightly larger than the combined ios
        dimension = (
                ioStageWidthDimension * len(self.ios) * 1.05
                + (len(self.ios) - 1) * horizontalDistanceBetweenIosDimension
        )
        return f'{dimension}{ioStageWidthUnit}'

    @property
    def stageNumber(self) -> str:
        return str(self.pipeline.stages.index(self) + 1)

    @property
    def lowestOverallJobId(self) -> str:
        maxNumberOfJobs = max(len(stage.jobs) for stage in self.pipeline.stages)
        for stage in self.pipeline.stages:
            if len(stage.jobs) == maxNumberOfJobs:
                return stage.jobs[-1].jobId

    @property
    def previousStageId(self) -> str:
        if self.isFirst:
            return 'start'
        return self.pipeline.stages[self.pipeline.stages.index(self) - 1].stageId

    @property
    def nextStageId(self) -> str:
        if self.isLast:
            return 'end'
        return self.pipeline.stages[self.pipeline.stages.index(self) + 1].stageId

    @property
    def isFirst(self) -> bool:
        return self.pipeline.stages.index(self) == 0

    @property
    def isLast(self) -> bool:
        return self.pipeline.stages.index(self) == len(self.pipeline.stages) - 1

    @property
    def hasIos(self) -> bool:
        return len(self.ios) > 0


class Jobs(MutableSequence):

    def __init__(self):
        super().__init__()
        self._jobs = []

    def __getitem__(self, index: int):
        return self._jobs[index]

    def __setitem__(self, index: int, item):
        self._jobs[index] = item

    def __delitem__(self, index: int):
        del self._jobs[index]

    def __len__(self):
        return len(self._jobs)

    def insert(self, index: int, item):
        self._jobs.insert(index, item)

    def by_id(self, jobId) -> Job:
        for job in self._jobs:
            if job.jobId != jobId:
                continue
            return job
        raise ValueError(jobId)

    def to_mustache(self) -> list:
        data = []
        for job in self._jobs:
            data.append(job.to_mustache())
        return data


class Job:

    def __init__(self, pipeline: Pipeline, stage: Stage):
        self.pipeline = pipeline
        self.stage = stage
        self.name = None
        self.jobId = None

    @property
    def previousJobId(self) -> str:
        if self.isFirst:
            return 'NO_PREVIOUS_JOB'
        return self.stage.jobs[self.stage.jobs.index(self) - 1].jobId

    @property
    def nextJobId(self) -> str:
        if self.isLast:
            return 'NO_NEXT_JOB'
        return self.stage.jobs[self.stage.jobs.index(self) + 1].jobId

    @property
    def isFirst(self) -> bool:
        return self.stage.jobs.index(self) == 0

    @property
    def isLast(self) -> bool:
        return self.stage.jobs.index(self) == len(self.stage.jobs) - 1

    def to_mustache(self) -> dict:
        return {
            **self.pipeline.dimensions.to_mustache(),
            'stage_name': self.stage.name,
            'job_name': self.name,
            'job_id': self.jobId,
            'is_first_job': self.isFirst,
            'is_last_job': self.isLast,
            'previous_stage_id': self.stage.previousStageId,
            'next_stage_id': self.stage.nextStageId,
            'previous_job_id': self.previousJobId,
            'next_job_id': self.nextJobId,
            'offset_from_previous_stage':
                self.pipeline.dimensions.horizontalDistanceBetweenStartAndFirstStage if self.stage.isFirst
                else self.pipeline.dimensions.horizontalDistanceBetweenStages,
        }


class Ios(MutableSequence):

    def __init__(self):
        super().__init__()
        self._ios = []

    def __getitem__(self, index: int):
        return self._ios[index]

    def __setitem__(self, index: int, item):
        self._ios[index] = item

    def __delitem__(self, index: int):
        del self._ios[index]

    def __len__(self):
        return len(self._ios)

    def insert(self, index: int, item):
        if len(self._ios) >= 3:
            raise ValueError('Cannot support more than three ios.')
        self._ios.insert(index, item)

    def by_id(self, ioId) -> Job:
        for io in self._ios:
            if io.ioId != ioId:
                continue
            return io
        raise ValueError(ioId)

    def to_mustache(self) -> list:
        data = []
        for io in self._ios:
            data.append(io.to_mustache())
        return data


class Io:

    _CYLINDER = 'Cylinder'
    _CLOUD = 'Cloud'
    _INPUT = 'Input'
    _OUTPUT = 'Output'

    def __init__(self, pipeline: Pipeline, stage: Stage):
        self.pipeline = pipeline
        self.stage = stage
        self.name = None
        self.ioId = None
        self.image = None
        self._shape = None
        self._direction = None

    @property
    def shape(self):
        return self._shape

    @shape.setter
    def shape(self, value):
        if value not in [self._CYLINDER, self._CLOUD]:
            raise ValueError(value)
        self._shape = value

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, value):
        if value not in [self._INPUT, self._OUTPUT]:
            raise ValueError(value)
        self._direction = value

    @property
    def setImageWidth(self) -> bool:
        return self.imageAspectRatio >= 1

    @property
    def setImageHeight(self) -> bool:
        return self.imageAspectRatio < 1

    @property
    def imageWidth(self) -> str:
        dimension, unit = split_dimension_and_unit(self.pipeline.dimensions.ioStageWidth)
        return f'{dimension * 0.5}{unit}'

    @property
    def imageHeight(self) -> str:
        dimension, unit = split_dimension_and_unit(self.pipeline.dimensions.ioStageHeight)
        return f'{dimension * 0.5}{unit}'

    @property
    def imageAspectRatio(self) -> float:
        with open(os.path.join(self.pipeline.outputPath, self.image), 'rb') as file:
            pdf = PdfFileReader(file)
            mediaBox = pdf.getPage(0).mediaBox
            return mediaBox.getWidth() / mediaBox.getHeight()

    def to_mustache(self) -> dict:
        return {
            **self.pipeline.dimensions.to_mustache(),
            'is_cylinder': self.shape == self._CYLINDER,
            'is_cloud': self.shape == self._CLOUD,
            'io_id': self.ioId,
            'is_input': self.direction == self._INPUT,
            'is_output': self.direction == self._OUTPUT,
            'image': self.image.replace('\\', '/'),
            'set_image_width': self.setImageWidth,
            'set_image_height': self.setImageHeight,
            'image_width': self.imageWidth,
            'image_height': self.imageHeight,
            'io_name': self.name,
        }


def split_dimension_and_unit(dimensionWithUnit: str) -> Tuple[float, str]:

    PATTERN = re.compile(r'^(?P<dimension>.*?)(?P<unit>(?:cm))$')

    match = re.search(PATTERN, dimensionWithUnit)

    if match is not None:
        return float(match.group('dimension')), match.group('unit')
    else:
        raise ValueError(dimensionWithUnit)
